package eventListenerNada

import (
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/tekion/development/tap/iot/mqttmoko/models"
)

type eventListenerNada struct {
	clients map[int64]mqtt.Client
}

type EventListenerNada interface {
	ProcessMocoIOtCoreMessage(temp map[string]interface{}) error
	ProcessMOCOMessage(topic string, mocoGen models.MocoGenerated)
}

func New() EventListenerNada {
	m := make(map[int64]mqtt.Client)
	return &eventListenerNada{clients: m}
}

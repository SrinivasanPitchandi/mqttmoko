package eventListenerNada

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/tekion/development/tap/iot/mqttmoko/Decode"
	"gitlab.com/tekion/development/tap/iot/mqttmoko/models"
	"strings"
)

func (e *eventListenerNada) ProcessMocoIOtCoreMessage(temp map[string]interface{}) error {
	fmt.Println()
	if _, ok := temp["encodedData"]; ok {
		e.ProcessEncodedMocoIOtCoreMessage(temp)
		return nil
	}
	return nil
}

func (e *eventListenerNada) ProcessEncodedMocoIOtCoreMessage(temp map[string]interface{}) {
	topic, _ := temp["topic"].(string)
	encodecStr, _ := temp["encodedData"].(string)
	rawDecodedData, err := base64.StdEncoding.DecodeString(encodecStr)
	if err != nil {
		fmt.Println(err)
	}
	e.handleReceivedMessage(rawDecodedData, topic)
}

func (e *eventListenerNada) handleReceivedMessage(rawDecodedData []byte, topic string) {
	fmt.Println("rawDecoded", rawDecodedData)
	if strings.Contains(string(rawDecodedData), "WirelessMetadata") {
		var generateMoco models.MocoGenerated
		if err := json.Unmarshal(rawDecodedData, &generateMoco); err != nil {
			fmt.Println(err)
		}

		e.ProcessMOCOMessage(topic, generateMoco)
	}
}

func (e *eventListenerNada) ProcessMOCOMessage(topic string, mocoGen models.MocoGenerated) {
	var maxRSSI int
	var macWithMaxRSSI string
	init := false
	bytesData := Decode.Base64ToBytes(mocoGen.PayloadData)
	if mocoGen.WirelessMetadata.LoRaWAN.FPort == 1 {
		decode := Decode.DecoderTag(mocoGen.WirelessMetadata.LoRaWAN.FPort, bytesData)
		var mocoHB = models.BleMokoHeartBeat{
			ActivityCount:   decode["activity_count"].(int),
			FirmwareVersion: decode["firmware_version"].(string),
			PayloadType:     decode["payload_type"].(string),
			RebootReason:    decode["reboot_reason"].(string),
		}
		fmt.Println("MOKOHeartBeat", mocoHB)
	}
	if mocoGen.WirelessMetadata.LoRaWAN.FPort == 2 {
		decode := Decode.DecoderTag(mocoGen.WirelessMetadata.LoRaWAN.FPort, bytesData)
		var macData []models.MacData
		mac := decode["mac_data"].([]map[string]interface{})

		for _, data := range mac {

			var scanIdx models.MacData
			rssi := data["rssi"].(int)
			if !init || rssi > maxRSSI {
				maxRSSI = rssi
				macWithMaxRSSI = fmt.Sprintf("%v", data["mac"])
				init = true
			}
			scanIdx.Mac = fmt.Sprintf("%v", data["mac"])
			scanIdx.Rssi, _ = data["rssi"].(int)
			macData = append(macData, scanIdx)
		}
		var mocoLoc = models.BleMokoLocation{
			MacData:             macData,
			PayloadType:         decode["payload_type"].(string),
			Port:                decode["port"].(int),
			PositionSuccessType: decode["position_success_type"].(string),
			Timestamp:           decode["timestamp"].(string),
		}
		fmt.Println("MokoLoc", mocoLoc)
		fmt.Println("MacWithMaximumRsi", macWithMaxRSSI)

	}
}

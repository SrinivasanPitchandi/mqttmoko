package main

import (
	"encoding/json"
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/tekion/development/tap/iot/mqttmoko/eventListenerNada"
	"gitlab.com/tekion/development/tap/iot/mqttmoko/models"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

func main() {
	broker := "tcp://52.35.202.114:1883" // MQTT broker URL and port
	clientID := generateRandomClientID() // Unique client ID for your application
	topic := "iot/moco/test"             // MQTT topic you want to subscribe to and publish to

	opts := mqtt.NewClientOptions().AddBroker(broker)
	opts.SetClientID(clientID)
	client := mqtt.NewClient(opts)
	eve := eventListenerNada.New()

	if token := client.Connect(); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}
	defer client.Disconnect(250)

	// Publish a message
	message := "Hello, MQTT!"
	token := client.Publish(topic, 0, false, message)
	token.Wait()

	// Subscribe to messages
	if token := client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {
		var generateMoco models.MocoGenerated
		if err := json.Unmarshal(msg.Payload(), &generateMoco); err != nil {
			//e.log.Error(ctx, errors.New("invalid topic"), iLog.Fields{"payload": string(msg.Payload()), "topic": msg.Topic()})
		}
		fmt.Println("generate", generateMoco)
		go eve.ProcessMOCOMessage(msg.Topic(), generateMoco)
		//fmt.Printf("Received message on topic %s: %s\n", msg.Topic(), msg.Payload())
	}); token.Wait() && token.Error() != nil {
		log.Fatal(token.Error())
	}

	// Handle interrupt signals and keep the program running
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}

func generateRandomClientID() string {
	// Set a seed for random number generation based on the current time
	rand.Seed(time.Now().UnixNano())

	// Define the characters that can be used in the client ID
	chars := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// Set the desired length of the client ID
	length := 10

	// Generate a random client ID
	clientID := make([]byte, length)
	for i := range clientID {
		clientID[i] = chars[rand.Intn(len(chars))]
	}

	return string(clientID)
}

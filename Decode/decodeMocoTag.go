package Decode

import (
	"fmt"
	"strconv"
	"strings"
)

func BytesToInt(bytes []byte, start int, length int) int {
	value := 0
	for i := 0; i < length; i++ {
		m := ((length - 1) - i) * 8
		value |= int(bytes[start+i]) << m
	}
	return value
}

func SubstringBytes(bytes []byte, start int, length int) string {
	char := []string{}
	for i := 0; i < length; i++ {
		char = append(char, fmt.Sprintf("%02X", bytes[start+i]))
	}
	return strings.Join(char, "")
}

func BytesToHexString(bytes []byte, start int, length int) string {
	char := []string{}
	for i := 0; i < length; i++ {
		dataHexStr := fmt.Sprintf("%02x", bytes[start+i])
		char = append(char, dataHexStr)
	}
	return strings.Join(char, "")
}

func SignedHexToInt(hexStr string) string {
	value, _ := strconv.ParseInt(hexStr, 16, 32)
	if value >= 0x80000000 {
		value = value - 0x100000000
	}
	return strconv.Itoa(int(value))
}

func DecoderTag(port int, bytes []byte) map[string]interface{} {
	payloadTypeArray := []string{"Heartbeat", "Location Fixed", "Location Failure", "Shutdown", "Shock", "Man Down detection", "Tamper Alarm", "Event Message", "Battery Consumption", "", "", "GPS Limit"}
	rebootReasonArray := []string{"Restart after power failure", "Bluetooth command request", "LoRaWAN command request", "Power on after normal power off"}
	positionTypeArray := []string{"WIFI positioning success", "Bluetooth positioning success", "GPS positioning success"}
	eventTypeArray := []string{
		"Start of movement",
		"In movement",
		"End of movement",
		"Uplink Payload triggered by downlink message",
	}

	dev_info := make(map[string]interface{})
	dev_info["port"] = port
	dev_info["payload_type"] = payloadTypeArray[port-1]
	if port == 1 {
		rebootReasonCode := BytesToInt(bytes, 3, 1)
		dev_info["reboot_reason"] = rebootReasonArray[rebootReasonCode]
		majorVersion := (bytes[4] >> 6) & 0x03
		minorVersion := (bytes[4] >> 4) & 0x03
		patchVersion := bytes[4] & 0x0f
		firmwareVersion := "V" + strconv.Itoa(int(majorVersion)) + "." + strconv.Itoa(int(minorVersion)) + "." + strconv.Itoa(int(patchVersion))
		dev_info["firmware_version"] = firmwareVersion
		activityCount := BytesToInt(bytes, 5, 4)
		dev_info["activity_count"] = activityCount
	} else if port == 2 {
		parse_len := 3
		datas := []map[string]interface{}{}
		positionTypeCode := bytes[parse_len]
		dev_info["position_success_type"] = positionTypeArray[positionTypeCode]
		year := int(bytes[parse_len+1])*256 + int(bytes[parse_len+2])
		parse_len += 2
		mon := bytes[parse_len+1]
		days := bytes[parse_len+2]
		hour := bytes[parse_len+3]
		minute := bytes[parse_len+4]
		sec := bytes[parse_len+5]
		timezone := bytes[parse_len+6]
		if timezone > 0x80 {
			dev_info["timestamp"] = fmt.Sprintf("%d-%d-%d %d:%d:%d  TZ:%d", year, mon, days, hour, minute, sec, int(timezone)-0x100)
		} else {
			dev_info["timestamp"] = fmt.Sprintf("%d-%d-%d %d:%d:%d  TZ:%d", year, mon, days, hour, minute, sec, timezone)
		}
		datalen := bytes[parse_len+7]
		if positionTypeCode == 0 || positionTypeCode == 1 {
			for i := 0; i < (int(datalen) / 7); i++ {
				data := map[string]interface{}{}
				data["mac"] = strings.ToLower(SubstringBytes(bytes, parse_len+8, 6))
				parse_len += 6
				data["rssi"] = int(bytes[parse_len+1]) - 256
				datas = append(datas, data)
			}
			dev_info["mac_data"] = datas
		} else {
			lat := BytesToInt(bytes, parse_len+8, 4)
			parse_len += 4
			lon := BytesToInt(bytes, parse_len+8, 4)
			parse_len += 4
			if lat > 0x80000000 {
				lat = lat - 0x100000000
			}
			if lon > 0x80000000 {
				lon = lon - 0x100000000
			}
			dev_info["latitude"] = lat / 10000000
			dev_info["longitude"] = lon / 10000000
			dev_info["pdop"] = bytes[parse_len+1] / 10
		}
	} else if port == 8 {
		eventTypeCode := int(bytes[3])
		// data.event_type_code = eventTypeCode;
		dev_info["event_type"] = eventTypeArray[eventTypeCode]
	}
	return dev_info
}

package Decode

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
)

func DecoderAP(fport int, bytes []byte) map[string]interface{} {
	flag := 0x1F
	region := []string{"AS923", "AU915", "CN470", "CN779", "EU433", "EU868", "KR920", "IN865", "US915", "RU864"}
	dev_info := make(map[string]interface{})
	if fport == 1 {
		dev_info["batt_level"] = fmt.Sprintf("%d%%", bytes[0])
		dev_info["batt_v"] = fmt.Sprintf("%dmV", int(bytes[1])*256+int(bytes[2]))
		ver_major := (bytes[3] >> 6) & 0x03
		ver_minor := (bytes[3] >> 4) & 0x03
		ver_patch := bytes[3] & 0x0f
		dev_info["ver"] = fmt.Sprintf("V%d.%d.%d", ver_major, ver_minor, ver_patch)
		dev_info["sensitivity"] = fmt.Sprintf("%dmg", bytes[4])
		dev_info["demolition_state"] = bytes[5]
		temperature := int(bytes[6])*256 + int(bytes[7])
		if temperature > 0x8000 {
			dev_info["temperature"] = fmt.Sprintf("-%.2f°C", float64(0x10000-temperature)/100)
		} else {
			dev_info["temperature"] = fmt.Sprintf("%.2f°C", float64(temperature)/100)
		}
		dev_info["humidity"] = fmt.Sprintf("%.2f%%", float64(int(bytes[8])*256+int(bytes[9]))/100)
		fmt.Println(len(region))
		if len(region) < int(bytes[10]) {
			dev_info["region"] = "undefined"
		} else {
			dev_info["region"] = region[bytes[10]]
		}

	} else if fport == 2 {
		dev_info["head"] = bytes[0]
		dev_info["beacon_num"] = bytes[1]
		parse_len := 2
		datas := []map[string]interface{}{}
		for i := 0; i < int(bytes[1]); i++ {
			data := make(map[string]interface{})
			beacon_len := 0
			current_data_len := bytes[parse_len]
			parse_len++
			if flag&0x10 != 0 {
				year := int(bytes[parse_len])*256 + int(bytes[parse_len+1])
				parse_len += 2
				mon := bytes[parse_len]
				parse_len++
				days := bytes[parse_len]
				parse_len++
				hour := bytes[parse_len]
				parse_len++
				minute := bytes[parse_len]
				parse_len++
				sec := bytes[parse_len]
				parse_len++
				data["utc_time"] = fmt.Sprintf("%d-%d-%d %d:%d:%d", year, mon, days, hour, minute, sec)
				beacon_len += 7
			}
			if flag&0x08 != 0 {
				data["mac"] = hex.EncodeToString(bytes[parse_len : parse_len+6])
				parse_len += 6
				beacon_len += 6
			}
			if flag&0x04 != 0 {
				data["rssi"] = fmt.Sprintf("%ddBm", int(bytes[parse_len])-256)
				parse_len++
				beacon_len += 1
			}
			if flag&0x03 != 0 {
				adv_len := int(current_data_len) - int(beacon_len)
				data["adv_len"] = adv_len
				data["adv_data"] = hex.EncodeToString(bytes[parse_len : parse_len+adv_len])
				parse_len += adv_len
			}
			datas = append(datas, data)
		}
		dev_info["scan_data"] = datas
	}
	return dev_info
}

//func HexToBytes(hex string) []byte {
//	bytes := make([]byte, 0)
//	count := 0
//	for i := 0; i < len(hex); i += 2 {
//		b, _ := base64.StdEncoding.DecodeString(hex)
//		bytes = append(bytes, b[count])
//		count += 1
//	}
//	return bytes
//}
//
//func Base64ToHex(base64String string) string {
//	binaryString, _ := base64.StdEncoding.DecodeString(base64String)
//	uint8Array := make([]byte, len(binaryString))
//	for i := 0; i < len(uint8Array); i++ {
//		uint8Array[i] = binaryString[i]
//	}
//	hexString := hex.EncodeToString(uint8Array)
//	return hexString
//}

func Base64ToBytes(base64String string) []byte {
	binaryString, _ := base64.StdEncoding.DecodeString(base64String)
	return binaryString
}

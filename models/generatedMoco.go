package models

import "time"

type MocoGenerated struct {
	MessageId        string `json:"MessageId"`
	WirelessDeviceId string `json:"WirelessDeviceId"`
	PayloadData      string `json:"PayloadData"`
	WirelessMetadata struct {
		LoRaWAN struct {
			ADR       bool   `json:"ADR"`
			Bandwidth int    `json:"Bandwidth"`
			ClassB    bool   `json:"ClassB"`
			CodeRate  string `json:"CodeRate"`
			DataRate  string `json:"DataRate"`
			DevAddr   string `json:"DevAddr"`
			DevEui    string `json:"DevEui"`
			FCnt      int    `json:"FCnt"`
			FOptLen   int    `json:"FOptLen"`
			FPort     int    `json:"FPort"`
			Frequency string `json:"Frequency"`
			Gateways  []struct {
				GatewayEui string  `json:"GatewayEui"`
				Rssi       int     `json:"Rssi"`
				Snr        float64 `json:"Snr"`
			} `json:"Gateways"`
			MIC                   string    `json:"MIC"`
			MType                 string    `json:"MType"`
			Major                 string    `json:"Major"`
			Modulation            string    `json:"Modulation"`
			PolarizationInversion bool      `json:"PolarizationInversion"`
			SpreadingFactor       int       `json:"SpreadingFactor"`
			Timestamp             time.Time `json:"Timestamp"`
		} `json:"LoRaWAN"`
	} `json:"WirelessMetadata"`
	Timestamp  int64  `json:"timestamp"`
	DeviceType string `json:"deviceType"`
}

package models

type MocoLocation struct {
	Head      uint8      `json:"head"`
	BeaconNum uint8      `json:"beacon_num"`
	ScanData  []ScanData `json:"scan_data"`
}

type ScanData struct {
	UtcTime string `json:"utc_time"`
	Mac     string `json:"mac"`
	Rssi    string `json:"rssi"`
	AdvLen  int    `json:"adv_len"`
	AdvData string `json:"adv_data"`
}
type BleMokoLocation struct {
	MacData             []MacData `json:"mac_data"`
	PayloadType         string    `json:"payload_type"`
	Port                int       `json:"port"`
	PositionSuccessType string    `json:"position_success_type"`
	Timestamp           string    `json:"timestamp"`
}
type MacData struct {
	Mac  string `json:"mac"`
	Rssi int    `json:"rssi"`
}

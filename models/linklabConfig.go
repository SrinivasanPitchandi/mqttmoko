package models

import "time"

type LinklabConfig struct {
	IsActive                   bool
	CreatedAt                  int64
	CreatedBy                  string
	UpdatedAt                  int64
	UpdatedBy                  string
	Id                         int64 `gorm:"primaryKey;autoIncrement:true"`
	TenantID                   string
	DealerID                   string
	SiteID                     string `json:"site_id"`
	SiteName                   string
	AreaName                   string
	LinklabSiteID              string
	LinklabAreaName            string
	LinklabMqttUser            string
	LinklabMqttPassword        string
	LinklabMqttClientId        string
	MqttTopic                  string
	LastMessageUpdate          string
	MessageFrequencyInLastHour string
	LastError                  string
	LastConnected              time.Time
	LastDisconnected           time.Time
	Status                     string
	Url                        string
	IsBool                     bool    `json:"isBool"`
	EventTime                  float64 `json:"eventTime"`
	LinklabIntegrationUser     string
	LinklabIntegrationPassword string
	APIUrl                     string
	IsApi                      bool    `json:"isApi"`
	ApiEventTime               float64 `json:"apiEventTime"`
	IsMqtt                     bool    `json:"isMqtt"`
	IsZone                     bool
	IsRfid                     bool   `json:"isRfid"`
	IsMoco                     bool   `json:"isMoco"`
	SqsQueueRfid               string `json:"sqsQueueRfid"`
	SqsQueueMoco               string `json:"sqsQueueMoco"`
	IsApMoko                   bool   `json:"isApMoko"`
}

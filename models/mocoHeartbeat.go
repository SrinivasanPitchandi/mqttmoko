package models

type MocoHeartBeat struct {
	BattLevel       string      `json:"batt_level"`
	BattV           string      `json:"batt_v"`
	Ver             string      `json:"ver"`
	Sensitivity     string      `json:"sensitivity"`
	DemolitionState uint8       `json:"demolition_state"`
	Temperature     string      `json:"temperature"`
	Humidity        string      `json:"humidity"`
	Region          interface{} `json:"region"`
}
type BleMokoHeartBeat struct {
	ActivityCount   int    `json:"activity_count"`
	FirmwareVersion string `json:"firmware_version"`
	PayloadType     string `json:"payload_type"`
	RebootReason    string `json:"reboot_reason"`
}
